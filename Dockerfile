FROM node:10.4.1-stretch
WORKDIR /app
COPY app.js /app/
CMD [ "node", "app.js" ]